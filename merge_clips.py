from moviepy.editor import *
video_dir='/home/yitzhak/video_lab/'
min_idx=0
max_idx=1
parts=[]
separator='separator_v2.mp4'
for idx in range(min_idx,max_idx+1):
    parts.append(VideoFileClip(video_dir +'part_'+str(idx)+'.mp4'))
    if (not (separator is None)) and (idx<max_idx):
        parts.append(VideoFileClip(video_dir+separator))
final_clip=concatenate_videoclips(parts)
final_clip.write_videofile(video_dir+'edited.mp4')

